<?php
include("conexion.php");
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
session_start();
if(isset($_GET['id'])){
  $idpub=$_GET['id'];
  $sql="SELECT * FROM publicacion WHERE ID_PUBLICACION=$idpub";
  $result=mysqli_query($con,$sql);
 
  $mostrar=mysqli_fetch_array($result);
  $idp=$mostrar['ID_PUBLICACION'];

}
?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <title>PUBLICACION</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" type="text/css" href="fondo.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
  <script src="tabla.js"></script>
  <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>

 
  </head>
  
  <body id="bodys" style="color:#000 !important; ">
  <nav id="navb" class="navbar navbar-expand-sm bg-dark navbar-dark">
  <!-- Brand -->
  <a class="active" href="index.php">Te sirve? Te lo regalo!</a>



  <!-- Links -->
  <ul class="navbar-nav">
  <?php
 require_once ("verlogin.php");   
 if ($estado){                    
?>

 
    <li class="nav-item">
      <a class="nav-link" href="#">Publicar!</a>
    </li>
    

    <!-- Dropdown -->
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" style="float:right;"href="#" id="navbardrop" data-toggle="dropdown">
        Mi Cuenta
      </a>
      <div  class="dropdown-menu">
        <a  class="dropdown-item submen" href="perfil.php">Perfil</a>
        <a  class="dropdown-item submen" href="listasolicitudes.php">Solicitudes</a>
        <a  class="dropdown-item submen" href="cerrarsesion.php">Cerrar Sesion</a>
      </div>
    </li>

    <?php
    }else {
    ?>
    <li class="nav-item">
      <a class="nav-link" href="iniciarsesion.html">Iniciar Sesión</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="registrar.html">Registrate!</a>
    </li>
    <?php } ?>

    <li class="nav-item">
    <a class="nav-link icono" href="#" >
     <img src="imagenes/instragram2.png" class="img-fluid"  width="30" height="30" alt="instragram">
    </a>
    </li>
    <li class="nav-item">
            <a class="nav-link icono" style="color: white;" href=""><img src="imagenes/fb3.png" class="img-fluid rounded" width="30" height="30" alt="facebook"></a>
    </li>
    <li class="nav-item">
            <a class="nav-link icono" style="color: white;" href="https://api.whatsapp.com/send?phone=56962423373&text=Te%20sirve?%20Te%20lo%20regalo!%20Hola,%20tengo%20una%20consulta."><img src="imagenes/ws.png" class="img-fluid" width="30" height="30" alt="whatsapp"></a>
    </li>
  </ul>
</nav>





    <div class="container">
      <div class="row " style="padding-top: 50px;">
        
        <div class="col-8 col-md-8 col-sm-12" >
          <form action="pedir.php" method="POST" enctype="multipart/form-data">
            <br>
          <div class="form-row" style="">
            
            <div class="col-12 col-md-12 col-sm-12 border"  style="text-align: center; background-color: #e2e2e2; "  >
              <h2 ><?php echo $mostrar['TITULO']?></h2>
            </div>
            
          </div>
          <br>
          <div class="form-row" style="" > 
            
            <div class="col-12 col-sm-12 col-md-12" style="text-align: center; background-color: #f7f7f7;" >
            
              <?php 
                              
                              $sql2="SELECT * FROM img WHERE ID_PUBLICACION=$idpub";
                              $result2=mysqli_query($con,$sql2);
                              $mostrar2=mysqli_fetch_array($result2); 
                              $idfoto=$mostrar2['idImagen'];
                              ?>
              <img src="ver-imagen.php?id=<?php echo $idfoto ?>"  width="500" height="500" class="image w3-border w3-padding">

            </div>
            </div>
        </form>
        </div>
        
        <?php
        $idsub=$mostrar['ID_SUBCATEGORIA'];
        $sql3="SELECT * FROM subcategoria WHERE ID_SUBCATEGORIA=$idsub";
        $result3=mysqli_query($con,$sql3);
        $mostrar3=mysqli_fetch_array($result3);
        $idcat=$mostrar3['ID_CATEGORIA'];
        $sql4="SELECT * FROM categoria WHERE ID_CATEGORIA=$idcat";
        $result4=mysqli_query($con,$sql4);
        $mostrar4=mysqli_fetch_array($result4);
        $idcom=$mostrar['ID_COMUNA'];
        $sql5="SELECT * FROM comuna WHERE ID_COMUNA=$idcom";
        $result5=mysqli_query($con,$sql5);
        $mostrar5=mysqli_fetch_array($result5);
        ?>
        <div class="col-4 col-md-4 col-sm-12" >
          <form action="pedir.php" method="POST" enctype="multipart/form-data">
            <br><br><br><br>
            
            <div class="card ">
              <div class="card-header" style="text-align:center;background-color: black;
    color: white;
"><h5>Información</h5></div>
              <div class="card-body">
              <p>Categoria: <?php echo $mostrar4['DESCRIPCION_C']?></p>
              <hr></hr>
              <p>Subcategoria: <?php echo$mostrar3['DESCRIPCION_S']?></p>
              <hr></hr>
              <p>Comuna: <?php echo$mostrar5['NOMBRE_C']?> </p>
              <hr>
              
                <div>
                <p>Descripción</p>
                </div>
                <p><?php echo $mostrar['DESCRIPCION_P'] ?></p>
                
              </div>
            </div>
            
            <br>

            <div class="form row">
             <button type="button" href="javascript:;" onclick="solicitar();return false;" class="btn btn-dark aqua-gradient btn-rounded" style=" margin:0 auto;" name="submit"  value="" >Solicitar donación</button>
            </div>
            <label id="resultado" name=codigo></label>
            <div>
              <input type="hidden" name="IDP" id="IDP" value=<?php echo $idp?>>
            </div>

          </form>
        </div>

        
      </div>



    </div>
          <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <footer class="page-footer font-small blue pt-4">

  <!-- Footer Links -->
  <div class="container-fluid text-center text-md-left">

    <!-- Grid row -->
    <div class="row" style="background-color: #231f20;
    color: white; padding-top:14px;">

      <!-- Grid column -->
      <div class="col-md-6 mt-md-0 mt-3">

        <!-- Content -->
        <h5 class="text-uppercase">Te sirve? Te lo regalo!</h5>
        <p>Nacimos de la necesidad de reclicar  y extender la vida útil de muchos objetos que son desechados dandole la oportunidad a alguien que necesite un objeto poder obtenerlo!
        </p>
        <p> Con tu donación no solo evitaras crear aún más basura, sino que podrás apoyar a una persona que lo necesite. Unete a nuestra camapaña!</p>

      </div>
      <!-- Grid column -->

      <hr class="clearfix w-100 d-md-none pb-3">

      <!-- Grid column -->
      <div class="col-md-3 mb-md-0 mb-3" style="padding: 0px 60px 0px 50px;">

        <!-- Links -->
        <img src="imagenes/recicla.jpg" class="img-fluid" style="max-width: 80%;height: auto;">

      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-3 mb-md-0 mb-3">

        <!-- Links -->
        <h5 class="text-uppercase">Contacto</h5>
        <address style="color: white;">
          <strong style="color: white;">Te sirve? Te lo regalo!</strong>
          <br style="color: white;">Concepción<br>
          <p style="color: white;">VIII Región del Bío-Bío</p>
          
          <a style="color: white;" href="https://api.whatsapp.com/send?phone=56962423373&text=Te%20sirve?%20Te%20lo%20regalo!%20Hola,%20tengo%20una%20consulta.">
          <abbr title="Phone"></abbr>
          <img src="imagenes/ws.png" class="img-fluid" alt="whatsapp" width="30" height="30">
          +569 62423373
        </a>
          <br>
          <abbr title="Email"></abbr>
          <a style="color: white;"href="mailto:contacto@tesirveteloregalo.cl">contacto@tesirveteloregalo.cl</a>
        </address>
        
       

      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row -->

  </div>
  <!-- Footer Links -->

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3" style="background-color:#121213; color:white;">© 2020 Copyright:
    <a style="color:white;" href="index.php"> Te sirve? Te lo regalo!</a>
  </div>
  <!-- Copyright -->

</footer>
  </body>
  <script>
window.onscroll = function() {myFunction()};

var navbar = document.getElementById("navb");
var sticky = navbar.offsetTop;

function myFunction() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("sticky")
  } else {
    navbar.classList.remove("sticky");
  }
}
</script>
<script type="text/javascript">
  function solicitar(){
    $.ajax({
      type:"POST",
      url:"pedir.php",
      data: "IDP="+$('#IDP').val(),
      success:function(r){
        if(r=="1"){
        alert("Error, Debe iniciar sesión para solicitar una donación");

        $('#resultado').html(r);
        }
        if(r=="2"){
        alert("Error, No puedes solicitar una publicación tuya");

        $('#resultado').html(r);
        }
        if(r=="3"){
        alert("Solicitud exitosa!");

        $('#resultado').html(r);
        }
        $('#resultado').html(r);
      }
    });
  }
</script>