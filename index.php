<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
header("Content-Type: text/html;charset=utf-8");
session_start();
include("conexion.php");

?>
<!DOCTYPE HTML>
<html lang="es">

<head>
	<title>Te sirve? Te lo regalo</title>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <body style="color:#000 !important;  background-color: #f7f7f7 !important">
    
           <div  class="col-sm-12 my-auto"  style="width: 80%; margin:0 auto;"> 
                    <div class="card"> 
                        <div class="card-header text-center">
                           <h2>Seleccione Ubicacion</h2>
                        </div>
                    <fieldset>
                        <div class="card-body">

<?php
 require_once ("verlogin.php");  //  verificamos si el usuario que esta visitando la pagina esta o no logeado 
 if ($estado){                   // si esta logiado vera esta informacion  
?>
 <div style="text-align: right;">
 <button type="button" class="btn btn-outline-primary" class="btn btn-link"  onclick="window.location='perfil.php' " style="position: relative; ;top: 30%;">PERFIL</button>
    <button type="button" class="btn btn-primary" class="btn btn-link"  onclick="window.location='cerrarsesion.php' " style="position: relative; ;top: 40%;">Cerrar Sesión</button>
</div>
<?php
 }else {
                         ///si no esta logeado vera esta informacion 

?>

<div style="text-align: right;">
    <button type="button" class="btn btn-outline-primary" class="btn btn-link"  onclick="window.location=' iniciarsesion.html ' " style="position: relative; ;top: 30%;">Iniciar Sesión</button>
    <button type="button" class="btn btn-primary" class="btn btn-link"  onclick="window.location='registrar.html' " style="position: relative; ;top: 40%;">Registrate</button>
    <?php
      }
       ?> 
</div>


  <form action="publicaciones.php" method="get">
    <div class="form-row"  >
    <div class="col-md-4">
		<div class="list-group">
      <?php
        $sql="SELECT * from region";
        $result=mysqli_query($con,$sql);
        while($mostrar=mysqli_fetch_array($result)){
        ?>
  			<a href="publicaciones.php?region=<?php echo $mostrar['Id_region']?>" class="list-group-item list-group-item-action"> <?php echo   $mostrar['nombre']?> </a>
  			<?php
      }
       ?> 
		</div>
	</div>
</form>
	<style>
		img {
			border-radius: 8px;
 			margin-left: auto;
  			margin-right: auto;
		}
	</style>
	<img src="mapa2.jpg" width="460" height="780">
</div>
</body>
