<?php
include("conexion.php");
session_start();
if(isset($_SESSION['RUT'])){
$id= $_SESSION['RUT'];}
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <title>Solicitudes donación</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" type="text/css" href="fondo.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
  <script src="tabla.js"></script>
  <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
</head>
<body id="bodys">
<nav id="navb" class="navbar navbar-expand-sm bg-dark navbar-dark">
  <!-- Brand -->
  <a class="active" href="index.php">Te sirve? Te lo regalo!</a>



  <!-- Links -->
  <ul class="navbar-nav">
  <?php
 require_once ("verlogin.php");   
 if ($estado){                    
?>

 
    <li class="nav-item">
      <a class="nav-link" href="#">Publicar!</a>
    </li>
    

    <!-- Dropdown -->
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" style="float:right;"href="#" id="navbardrop" data-toggle="dropdown">
        Mi Cuenta
      </a>
      <div  class="dropdown-menu">
        <a  class="dropdown-item submen" href="perfil.php">Perfil</a>
        <a  class="dropdown-item submen" href="listasolicitudes.php">Solicitudes</a>
        <a  class="dropdown-item submen" href="cerrarsesion.php">Cerrar Sesion</a>
      </div>
    </li>

    <?php
    }else {
    ?>
    <li class="nav-item">
      <a class="nav-link" href="iniciarsesion.html">Iniciar Sesión</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="registrar.html">Registrate!</a>
    </li>
    <?php } ?>

    <li class="nav-item">
    <a class="nav-link icono" href="#" >
     <img src="imagenes/instragram2.png" class="img-fluid"  width="30" height="30" alt="instragram">
    </a>
    </li>
    <li class="nav-item">
            <a class="nav-link icono" style="color: white;" href=""><img src="imagenes/fb3.png" class="img-fluid rounded" width="30" height="30" alt="facebook"></a>
    </li>
    <li class="nav-item">
            <a class="nav-link icono" style="color: white;" href="https://api.whatsapp.com/send?phone=56962423373&text=Te%20sirve?%20Te%20lo%20regalo!%20Hola,%20tengo%20una%20consulta."><img src="imagenes/ws.png" class="img-fluid" width="30" height="30" alt="whatsapp"></a>
    </li>
  </ul>
</nav>

<div>


<div> 
    
    <style>  .row{
    padding: 14px 0px 0px 0px;
 }  </style>
       
    <div style="padding:120px;">
        
        <div class="container-fluid border shadow" style="background-color:white;" >
            <table id="tabla" class="table table-hover">
                <thead>
                  <tr>
                    <th>Publicación</th>
                    <th>Solicitante</th>
                    <th>Teléfono</th>
                    <th>Email</th>
                  </tr>
                </thead>


                <tbody>
                <?php
                    if(isset($_SESSION['RUT'])){
                    $sql="SELECT * FROM publicacion WHERE RUT=$id";
                    
                    $result=mysqli_query($con,$sql);
                
                    while($mostrar=mysqli_fetch_array($result)){
                        $idpub=$mostrar['ID_PUBLICACION'];
                        $titulo=$mostrar['TITULO'];
                        $sql3="SELECT * FROM solicitantes where ID_PUBLICACION=$idpub";
                        $result3=mysqli_query($con,$sql3);
                        
                        if($mostrar3=mysqli_fetch_array($result3)){
                        $rut=$mostrar3['RUT'];

                        $sql2="SELECT * FROM usuario where RUT=$rut";
                        $result2=mysqli_query($con,$sql2);
                        if($mostrar2=mysqli_fetch_array($result2)){
                        $email=$mostrar2['EMAIL'];
                        $telefono=$mostrar2['TELEFONO'];
                        $nombre=$mostrar2['NOMBRE']." ".$mostrar2['APELLIDO'];
                    
                    
                ?>
                  <tr>
                    <td><?php echo $titulo ?></td>
                    <td><?php echo $nombre ?></td>
                    <td><?php echo $telefono ?></td>
                    <td><?php echo $email ?></td>
                  </tr>
                  

                    <?php  }}}}?>

                </tbody>
              </table>
        </div>
                           
    </div>

</div>
                    
<div>

<!-- Footer -->
<footer class="page-footer font-small blue pt-4">

  <!-- Footer Links -->
  <div class="container-fluid text-center text-md-left">

    <!-- Grid row -->
    <div class="row" style="background-color: #231f20;
    color: white;">

      <!-- Grid column -->
      <div class="col-md-6 mt-md-0 mt-3">

        <!-- Content -->
        <h5 class="text-uppercase">Te sirve? Te lo regalo!</h5>
        <p>Nacimos de la necesidad de reclicar  y extender la vida útil de muchos objetos que son desechados dandole la oportunidad a alguien que necesite un objeto poder obtenerlo!
        </p>
        <p> Con tu donación no solo evitaras crear aún más basura, sino que podrás apoyar a una persona que lo necesite. Unete a nuestra camapaña!</p>

      </div>
      <!-- Grid column -->

      <hr class="clearfix w-100 d-md-none pb-3">

      <!-- Grid column -->
      <div class="col-md-3 mb-md-0 mb-3" style="padding: 0px 60px 0px 50px;">

        <!-- Links -->
        <img src="imagenes/recicla.jpg" class="img-fluid" style="max-width: 80%;height: auto;">

      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-3 mb-md-0 mb-3">

        <!-- Links -->
        <h5 class="text-uppercase">Contacto</h5>
        <address style="color: white;">
          <strong style="color: white;">Te sirve? Te lo regalo!</strong>
          <br style="color: white;">Concepción<br>
          <p style="color: white;">VIII Región del Bío-Bío</p>
          
          <a style="color: white;" href="https://api.whatsapp.com/send?phone=56962423373&text=Te%20sirve?%20Te%20lo%20regalo!%20Hola,%20tengo%20una%20consulta.">
          <abbr title="Phone"></abbr>
          <img src="imagenes/ws.png" class="img-fluid" alt="whatsapp" width="30" height="30">
          +569 62423373
        </a>
          <br>
          <abbr title="Email"></abbr>
          <a style="color: white;"href="mailto:contacto@tesirveteloregalo.cl">contacto@tesirveteloregalo.cl</a>
        </address>
        
       

      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row -->

  </div>
  <!-- Footer Links -->

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3" style="background-color:#121213; color:white;">© 2020 Copyright:
    <a style="color:white;" href="index.php"> Te sirve? Te lo regalo!</a>
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->


</div>
</body>
</html>

<script type="text/javascript">
    $(document).ready(function() {
    $('#tabla').DataTable();
} );
  </script>

<script>
window.onscroll = function() {myFunction()};

var navbar = document.getElementById("navb");
var sticky = navbar.offsetTop;

function myFunction() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("sticky")
  } else {
    navbar.classList.remove("sticky");
  }
}
</script>