<?php

session_start();
$versesion = $_SESSION['RUT'];
if ($versesion == null || $versesion =''){         // funcion que permite solo ingresar al perfil si esta logeado 
  echo'<script type="text/javascript">
    alert("INICIE SESION PARA PODER INGRESAR");
    window.history.go(-1);
    </script>';        
   die();
}
include("conexion.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <title>NUEVA PUBLICACIÓN</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

</head>

<body style="color:#000 !important; background-color:#effafb !important">

<div class="container">
  <h1></h1>
  
  <form action="registrarpublicacion.php" class="was-validated" enctype="multipart/form-data" method="POST">
    <div class="container-fluid"> 
    
           <div  class="col-sm-12 my-auto"  style="width: 80%; margin:0 auto;"> 
                    <div class="card"> 
                        <div class="card-header text-center">
                           <h3>Registrar publicación</h3>
                        </div>
                    <fieldset>
                        <div class="card-body">

    <div class="form-row">
      <div class="col-md-6">
        <label for="user">TITULO:</label>
        <input type="text" class="form-control" id="user" placeholder="Televisor LED 4K poco uso" name="titulo" required>
        <div class="valid-feedback">Correcto</div>
        <div class="invalid-feedback">Rellene este campo</div>
      </div>
   
      <div class="col-md-6">
        <label for="sell">Estado:</label>
        <select type="text" class="form-control" id="sel1" name="estado"required>
          <option>NUEVO</option>
          <option>USADO</option>
        </select>
      </div>
    </div>

    <div class="form-row">
      <div class="col-md-6">
        <div>
           <label for="categoria">Region:</label>
        <select type="text" class="form-control" id="region" name="region"required>
          <?php
                          $sql1="SELECT * from region";
                          
          
                          $result1=mysqli_query($con,$sql1);
                          
                          while($mostrar=mysqli_fetch_array($result1)){
                           $id=$mostrar['Id_region'];?>
                              <option value=<?php echo $id?> > <?php echo $mostrar['nombre']?></option>

          <?php }?>
        </select> <br>

       <div id="comuna"></div>
       
       
      </div>
      </div>
    <div class="col-md-6">
        <label for="sell">Descripción:</label>
        <textarea class="form-control" rows="5" id="comment" placeholder="Caracteristicas del producto/servicio" name="descripcion"required></textarea>
      </div>

      
    </div> 
    <br>
     <div class="form-row">
      <div class="col-md-6">
        <label for="categoria">Categoria</label>
        <select type="text" class="form-control" id="sel1" name="subcategoria"required>
          <?php
                          $sql1="SELECT * from categoria";
                          
          
                          $result1=mysqli_query($con,$sql1);
                          
                          while($mostrar=mysqli_fetch_array($result1)){
                          ?>
                              <option  disabled><?php echo $mostrar['DESCRIPCION_C']?></option>
                             <?php 
                                  $cat=$mostrar['ID_CATEGORIA'];
                                  $sql2="SELECT * FROM subcategoria WHERE ID_CATEGORIA=$cat";
                                  $result2=mysqli_query($con,$sql2);
                                  while($mostrar2=mysqli_fetch_array($result2)){$id=$mostrar2['ID_SUBCATEGORIA'];?>
                                    <option value=<?php echo $id?>> <?php echo $mostrar2['DESCRIPCION_S']?></option> <?php }?>


          

          <?php }?>
        </select>
      </div>
      
      <div class="col-md-6">
       <label for="img">Imagen:</label>
        <input type="file" class="form-control-file border"  name="miArchivo" required>
      </div>
    </div>
      <br>

    <button type="submit" class="btn btn-primary" style=" margin:0 auto;" >Registrar</button>
    <input type="button" value="Cancelar" onclick="history.back() "/>

</div>
</div>
</div>
</form>
</body>
</html>
<script type="text/javascript">
  $(document).ready(function(){
    $('#region').val(0);
    recargarLista();

    $('#region').change(function(){
      recargarLista();
    });
  })
</script>
<script type="text/javascript">
  function recargarLista(){
    $.ajax({
      type:"POST",
      url:"datos.php",
      data:"region1=" + $('#region').val(),
      success:function(r){
        $('#comuna').html(r);
      }
    });
  }
</script>