<?php
session_start();
$versesion = $_SESSION['RUT'];
if ($versesion == null || $versesion =''){         // funcion que permite solo ingresar al perfil si esta logeado 
  echo'<script type="text/javascript">
    alert("INICIE SESION PARA PODER INGRESAR");
    window.location.href="index.php";
    </script>';        
   die();
}
$RUT=$_SESSION['RUT'];
$usuario= $_SESSION['usuario'];
include("conexion.php");

?>

<!DOCTYPE html>
<html lang="es">
<head>
  <title>Datos del Usuario</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container-fluid"> 
        <div  class="col-sm-12 my-auto"  style="width: 80%; margin:0 auto;"> 
            <div class="card"> 
                <div class="card-header text-center">
                    <h2>Informacion personal</h2>
                </div>
    <div class="card-body">


<form>
    <div class="form-row">
      <div class="col">
        <style>
			img {
			border-radius: 8px;
 			margin-left: auto;
  			margin-right: auto;
			}
		</style>
			<img src="perfil.png" class="rounded-circle" width="300" height="300">
      </div>
      <div class="col-sm-8">
        	<?php
    $consulta = mysqli_query ($con, "SELECT * FROM usuario WHERE RUT ='$RUT' LIMIT 1");
    		while($mostrar=mysqli_fetch_array($consulta)){
    		?>

	<div class="container">          
		<br><br>
  	<table class="table table-bordered">
    <thead>
        <tr class="table-dark text-dark">
        <th>Rut</th>
        <th>Nombre</th>
        <th>Apellido</th>
        
      </tr>
    </thead>
    <tbody>
        <tr>
        <td><?php echo $mostrar['RUT']?></td>
        <td><?php echo $mostrar['NOMBRE']?></td>
        <td><?php echo $mostrar['APELLIDO']?></td>
        
        </tr>
    </tbody>
  	</table>
	</div>

<div class="container">          
  	<table class="table table-bordered">
    <thead>
      <tr class="table-dark text-dark">
        <th>Usuario</th>
        <th>Email</th>
        <th>Fecha_nacimiento</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><?php echo $mostrar['NOMBRE_USUARIO']?></td>
        <td><?php echo $mostrar['EMAIL']?></td>
        <td><?php echo $mostrar['FECHA_NACIMIENTO']?></td>
      </tr>
    </tbody>
  	</table>
	</div>
			<?php
          }
       ?>
      </div>
    </div>
  </form>

  	<div style="text-align: left;">
    	
    	<button type="button" class="btn btn-primary" class="btn btn-link"  onclick="window.location='modificar.html' " style="position: relative; ;top: 40%;">Modificar</button>

    	<button type="button" class="btn btn-secundary" class="btn btn-link"  onclick="window.location='index.php' " style="position: relative; ;top: 40%;">Volver</button>
    	  	<div style="text-align: right;">
    				<button type="button" class="btn btn-primary" class="btn btn-link"  onclick="window.location='cerrarsesion.php' " style="position: relative; ;top: 40%;">Cerrar sesion</button>
			</div>
	</div>
</div>
</div>
</div>
</div>
</body>
</html>     