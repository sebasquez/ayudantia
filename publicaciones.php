<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
session_start();
$_session['pagina']='publicaciones.php';
include("conexion.php");

  if(isset($_GET['comuna'])&&$_GET['comuna']!=""){
    $comu=$_GET['comuna'];
  }
  if(isset($_GET['subcategoria'])){ 
    $subcat=$_GET['subcategoria'];
  }
  if(isset($_GET['region'])){
    $reg=$_GET['region']; 
}
 if(isset($_POST['comuna'])){
    $comu=$_POST['comuna'];
  }
  if(isset($_POST['subcategoria'])){
    $subcat=$_POST['subcategoria'];
  }
  if(isset($_POST['region'])){
    $reg=$_POST['region']; 
}
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Publicaciones</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" type="text/css" href="fondo.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
  <script src="tabla.js"></script>
  <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
  </head>
  <body id="bodys"style="color:#000 !important;">

    
  <nav id="navb" class="navbar navbar-expand-sm bg-dark navbar-dark">
  <!-- Brand -->
  <a class="active" href="index.php">Te sirve? Te lo regalo!</a>



  <!-- Links -->
  <ul class="navbar-nav">
  <?php
 require_once ("verlogin.php");   
 if ($estado){                    
?>

 
    <li class="nav-item">
      <a class="nav-link" href="#">Publicar!</a>
    </li>
    

    <!-- Dropdown -->
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" style="float:right;"href="#" id="navbardrop" data-toggle="dropdown">
        Mi Cuenta
      </a>
      <div  class="dropdown-menu">
        <a  class="dropdown-item submen" href="perfil.php">Perfil</a>
        <a  class="dropdown-item submen" href="listasolicitudes.php">Solicitudes</a>
        <a  class="dropdown-item submen" href="cerrarsesion.php">Cerrar Sesion</a>
      </div>
    </li>

    <?php
    }else {
    ?>
    <li class="nav-item">
      <a class="nav-link" href="iniciarsesion.html">Iniciar Sesión</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="registrar.html">Registrate!</a>
    </li>
    <?php } ?>

    <li class="nav-item">
    <a class="nav-link icono" href="#" >
     <img src="imagenes/instragram2.png" class="img-fluid"  width="30" height="30" alt="instragram">
    </a>
    </li>
    <li class="nav-item">
            <a class="nav-link icono" style="color: white;" href=""><img src="imagenes/fb3.png" class="img-fluid rounded" width="30" height="30" alt="facebook"></a>
    </li>
    <li class="nav-item">
            <a class="nav-link icono" style="color: white;" href="https://api.whatsapp.com/send?phone=56962423373&text=Te%20sirve?%20Te%20lo%20regalo!%20Hola,%20tengo%20una%20consulta."><img src="imagenes/ws.png" class="img-fluid" width="30" height="30" alt="whatsapp"></a>
    </li>
  </ul>
</nav>



      <div class="container-fluid"  >

          <div class="row" style="padding-top: 45px;">

              <div class="col-md-3 col-sm-12"style="padding-top: 44px;">
                <br><br>
                <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']) ?>" method="GET" enctype="multipart/form-data">
                <div class="row" >
                <div class="col-2 col-md-2 col-sm-2"> </div>

                  <div id="buscador" class="col-9 col-md-9 border " >
                    <div class="">
                      <label for="palabra">Palabra clave</label>

                 
                        <input class="form-control mb-4" id="tableSearch" type="text"
                          placeholder="Ingresa palabra">
                    </div>
                    <div>
                      <label for="palabra">Region</label>
                      <select type="text" class="form-control" id="region" name="region">
                          <?php
                          $sql1="SELECT * from region";
                          
          
                          $result1=mysqli_query($con,$sql1);
                          
                          while($mostrar=mysqli_fetch_array($result1)){
                           $id=$mostrar['Id_region'];?>
                              <option value=<?php echo $id?> > <?php echo $mostrar['nombre']?></option>

                            <?php }?>
                          </select>

                          <div id="comuna"></div>

                    </div>
                    <div>
                      <label for="catego">Categoria</label>
        <select type="text" class="form-control" id="categoria" name="categoria">
          
          <?php
                          $sql1="SELECT * from categoria";
                          
          
                          $result1=mysqli_query($con,$sql1);
                          
                          while($mostrar=mysqli_fetch_array($result1)){
                          ?>
                              <option value=<?php echo $mostrar['ID_CATEGORIA']?>> <?php echo $mostrar['DESCRIPCION_C']?></option>
                          


          

          <?php }?>

        </select>
                    </div>
                    <div id=subcategorias></div>
                    
               
                    <div style="text-align: center;padding: 10px 0px 10px 0px;">
                    <button type="submit" id="btnbuscar" class="btn btn-primary aqua-gradient btn-rounded" style=" margin:0 auto;" name="submit" value="buscar" >Buscar</button>
                     </div>

                  </div>

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 
                  <div class="col-3 col-md-3 col-sm-12"> </div>
                </div>
                
              </form>
              </div>


              <div class="col-md-9 col-sm-12">
                <div class="col-md-12 col-sm-12 col-12 table-responsive" style="    padding: 0px 35px 0px 0px;" >
                <table class="table table-hover display border" id="tablita" style="color:#000 ; background-color:#ffffff91; ">
                  <header> <h2 style="    padding-top: 20px;">Publicaciones</h2> </header>
                  <hr>
                    <thead>
                      <tr>
                        <th>Fecha</th>
                        <th>Imagen</th>
                        <th>Titulo</th>
                        <th >Categoria</th>
                        <th >Descripcion</th>

                      </tr>
                    </thead>
                    <tbody id="myTable">
                      <?php
                           $sql="SELECT *, DATE_FORMAT(FECHA_PUBLICACION,'%d/%m/%Y') AS FECHA_PUBLICACION from publicacion" ;
                           if(isset($reg)){
                              $consul="SELECT * FROM comuna WHERE ID_REGION=$reg";
                              $rel=mysqli_query($con,$consul);
                              $row=mysqli_fetch_array($rel);
                              $idcom=$row['ID_COMUNA'];
                              $sql="SELECT * , DATE_FORMAT(FECHA_PUBLICACION,'%d/%m/%Y') AS FECHA_PUBLICACION FROM publicacion WHERE ID_COMUNA=$idcom";
                            }
                          if(isset($comu) && isset($reg) && $comu!=''){
                            $sql="SELECT * , DATE_FORMAT(FECHA_PUBLICACION,'%d/%m/%Y') AS FECHA_PUBLICACION from publicacion WHERE ID_COMUNA=$comu";
                
                          }
                                            
                         if(isset($comu) && isset($subcat))
                          {
                            $sql="SELECT *, DATE_FORMAT(FECHA_PUBLICACION,'%d/%m/%Y') AS FECHA_PUBLICACION from publicacion WHERE ID_COMUNA=$comu AND ID_SUBCATEGORIA=$subcat";
                          }elseif (isset($comu)) {
                            $sql="SELECT *, DATE_FORMAT(FECHA_PUBLICACION,'%d/%m/%Y') AS FECHA_PUBLICACION from publicacion WHERE ID_COMUNA=$comu";
                          }elseif (isset($subcat)){
                            $sql="SELECT *, DATE_FORMAT(FECHA_PUBLICACION,'%d/%m/%Y') AS FECHA_PUBLICACION from publicacion WHERE ID_SUBCATEGORIA=$subcat";
                          } 
                          
                          
                          $sql=$sql." ORDER BY FECHA_PUBLICACION DESC";
          
                          $result=mysqli_query($con,$sql);
                          
                           while($mostrar=mysqli_fetch_array($result)){

                              $idpublicacion = $mostrar['ID_PUBLICACION'];

                              $sql2="SELECT * FROM img WHERE ID_PUBLICACION=$idpublicacion";
                              $result2=mysqli_query($con,$sql2);
                              $mostrar2=mysqli_fetch_array($result2); 
                              $idfoto=$mostrar2['idImagen'];
                          ?>
                     
                      <tr style="height: 145px;" href="publicacion.php?id=<?php echo $mostrar['ID_PUBLICACION']?>">
                         <a class="redirect-to-url" href="publicacion.php?id=<?php echo $mostrar['ID_PUBLICACION']?>">
                        <td style="padding:55px 10px">
                          
                          <?php echo $mostrar['FECHA_PUBLICACION']?>
                        </td>
                       
                        <?php 
                              
                              ?>
          
                        <td>
          
                           <a class="redirect-to-url" href="publicacion.php?id=<?php echo $mostrar['ID_PUBLICACION']?>">
                            <div class="image_border">
                              
                                
                               <img src="ver-imagen.php?id=<?php echo $idfoto ?>" border="1" width="120" height="120" class="image w3-border w3-padding">
                              
                               </a>
                             
                            
                          </div>

                          
                        </td>
                            
                        <td style="padding: 30px 0px 0px 10px;">
                        <?php
                                $texto2=$mostrar['TITULO'];
                                $newtext2 = wordwrap($texto2, 8, "\n", true);
                                $substring2=substr($texto2,0,38);
                           ?>
                        <?php echo $substring2?><p hidden><?php echo $mostrar['DESCRIPCION_P']?></p>
                       </td>
                        </a>

                        <td style="padding:55px 20px"><?php 
                        $idsub=$mostrar['ID_SUBCATEGORIA']; 
                        $sql5="SELECT * FROM subcategoria WHERE ID_SUBCATEGORIA=$idsub";
                        $result5=mysqli_query($con,$sql5);
                        $row5=mysqli_fetch_array($result5);
                        echo $row5['DESCRIPCION_S']?>
                          
                        </td>
                        <td style="padding-top: 30px;">
                          <?php
                                $texto=$mostrar['DESCRIPCION_P'];
                                $newtext = wordwrap($texto, 8, "\n", true);
                                $substring=substr($texto,0,200);
                           ?>
                          
                          <p class="font-weight-light small" ><?php echo $substring ?>...</p>
                          
                        </td>
                      </tr>
                    
                        <?php
                       }?>
                     
                    </tbody>
                  </table>
                  </div>
                      </div>
              </div>

          </div>
      </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <footer class="page-footer font-small blue pt-4">

  <!-- Footer Links -->
  <div class="container-fluid text-center text-md-left">

    <!-- Grid row -->
    <div class="row" style="background-color: #231f20;
    color: white; padding-top:14px;">

      <!-- Grid column -->
      <div class="col-md-6 mt-md-0 mt-3">

        <!-- Content -->
        <h5 class="text-uppercase">Te sirve? Te lo regalo!</h5>
        <p>Nacimos de la necesidad de reclicar  y extender la vida útil de muchos objetos que son desechados dandole la oportunidad a alguien que necesite un objeto poder obtenerlo!
        </p>
        <p> Con tu donación no solo evitaras crear aún más basura, sino que podrás apoyar a una persona que lo necesite. Unete a nuestra camapaña!</p>

      </div>
      <!-- Grid column -->

      <hr class="clearfix w-100 d-md-none pb-3">

      <!-- Grid column -->
      <div class="col-md-3 mb-md-0 mb-3" style="padding: 0px 60px 0px 50px;">

        <!-- Links -->
        <img src="imagenes/recicla.jpg" class="img-fluid" style="max-width: 80%;height: auto;">

      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-3 mb-md-0 mb-3">

        <!-- Links -->
        <h5 class="text-uppercase">Contacto</h5>
        <address style="color: white;">
          <strong style="color: white;">Te sirve? Te lo regalo!</strong>
          <br style="color: white;">Concepción<br>
          <p style="color: white;">VIII Región del Bío-Bío</p>
          
          <a style="color: white;" href="https://api.whatsapp.com/send?phone=56962423373&text=Te%20sirve?%20Te%20lo%20regalo!%20Hola,%20tengo%20una%20consulta.">
          <abbr title="Phone"></abbr>
          <img src="imagenes/ws.png" class="img-fluid" alt="whatsapp" width="30" height="30">
          +569 62423373
        </a>
          <br>
          <abbr title="Email"></abbr>
          <a style="color: white;"href="mailto:contacto@tesirveteloregalo.cl">contacto@tesirveteloregalo.cl</a>
        </address>
        
       

      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row -->

  </div>
  <!-- Footer Links -->

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3" style="background-color:#121213; color:white;">© 2020 Copyright:
    <a style="color:white;" href="index.php"> Te sirve? Te lo regalo!</a>
  </div>
  <!-- Copyright -->

</footer>
  </body>
</html>
<script type="text/javascript">
  $(document).ready(function(){
    $('#region').val(0);
    recargarLista();

    $('#region').change(function(){
      recargarLista();
    });
  })
</script>
<script type="text/javascript">
  function recargarLista(){
    $.ajax({
      type:"POST",
      url:"datos.php",
      data:"region1=" + $('#region').val(),
      success:function(r){
        $('#comuna').html(r);
      }
    });
  }
</script>

<script type="text/javascript">
  // Busqueda palabra clave

$(document).ready(function(){
  $("#tableSearch").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>



<script type="text/javascript">
  $(document).ready(function(){
    $('#categoria').val(1);
    recargarLista1();

    $('#categoria').change(function(){
      recargarLista1();
    });
  })
</script>
<script type="text/javascript">
  function recargarLista1(){
    $.ajax({
      type:"POST",
      url:"subcate.php",
      data:"categoriass=" + $('#categoria').val(),
      success:function(r){
        $('#subcategorias').html(r);
      }
    });
  }
</script>

<script type="text/javascript">
  $(document).ready(function(){
    $('#tablita').after('<div id="nav"></div>');
    var rowsShown = 4;
    var rowsTotal = $('#tablita tbody tr').length;
    var numPages = rowsTotal/rowsShown;
    for(i = 0;i < numPages;i++) {
        var pageNum = i + 1;
        $('#nav').append('<a href="#" class ="page_link active btn btn-primary btn-dark" rel="'+i+'">'+pageNum+'</a> ');
    }
    $('#tablita tbody tr').hide();
    $('#tablita tbody tr').slice(0, rowsShown).show();
    $('#nav a:first').addClass('active');
    $('#nav a').bind('click', function(){

        $('#nav a').removeClass('active');
        $(this).addClass('active');
        var currPage = $(this).attr('rel');
        var startItem = currPage * rowsShown;
        var endItem = startItem + rowsShown;
        $('#tablita tbody tr').css('opacity','0.0').hide().slice(startItem, endItem).
        css('display','table-row').animate({opacity:1}, 300);
    });
});
</script>

<script type="text/javascript">
  $('tr').on("click", function() { if($(this).attr('href') !== undefined){ document.location = $(this).attr('href'); } });


</script>
<script type="text/javascript">
function truncateText(selector, maxLength) {
    var element = document.querySelector(selector),
        truncated = element.innerText;

    if (truncated.length > maxLength) {
        truncated = truncated.substr(0,maxLength) + '...';
    }
    return truncated;
}

</script>
<script>
window.onscroll = function() {myFunction()};

var navbar = document.getElementById("navb");
var sticky = navbar.offsetTop;

function myFunction() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("sticky")
  } else {
    navbar.classList.remove("sticky");
  }
}
</script>